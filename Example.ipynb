{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "91b303de",
   "metadata": {},
   "source": [
    "This notebook demonstrates how ebweyl can be used to compute the electric and magnetic parts of the Weyl tensor of a given spacetime and classify it according to the Petrov types.\n",
    "\n",
    "In this process we will compute the spatial Ricci tensor and scalar as well as the Weyl scalar for an arbitrary tetrad base and the invariant scalars: I, J, K, L , N."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "52cfe47b",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import ebweyl as ebw"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d2beba4",
   "metadata": {},
   "source": [
    "The documentation on the ebweyl module is accessible with the help() function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "fdc742c0",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function ricci_tensor_down3 in module ebweyl:\n",
      "\n",
      "ricci_tensor_down3(self, Gudd3)\n",
      "    Compute spatial Ricci tensor with both indices down.\n",
      "    \n",
      "    Parameters : \n",
      "        Gudd3 : (3, 3, 3, N, N, N) array_like \n",
      "                Spatial Christoffel symbol with one indice up and two down\n",
      "            \n",
      "    Returns : \n",
      "        (3, 3, N, N, N) array_like\n",
      "\n"
     ]
    }
   ],
   "source": [
    "help(ebw.Weyl.ricci_tensor_down3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9571d5ea",
   "metadata": {},
   "source": [
    "# Define the Spacetime\n",
    "\n",
    "For this example we use the A.Harvey and T.Tsoubelis vacuum Bianchi IV plane wave homogeneous spacetime (see page 191 of 'Dynamical Systems in Cosmology' by J.Wainwright and G.F.R.Ellis). With Cartesian coordinates the metric takes the form:\n",
    "\n",
    "$$\n",
    "g_{\\alpha\\beta} = \n",
    "\\begin{pmatrix}\n",
    "   -1 &  0  & 0 & 0 \\\\\n",
    "    0 & t^2 & 0 & 0 \\\\\n",
    "    0 &  0  & te^{x} & te^{x}(x+\\log{t}) \\\\\n",
    "    0 &  0  & te^{x}(x+\\log{t}) & te^{x}((x+\\log{t})^2+1)\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "This is an analytical spacetime but the code presented here works for any numerical spacetime, the user needs the metric, extrinsic curvature and stress-energy tensor. \n",
    "\n",
    "For this metric the extrinsic curvature is: $K_{\\alpha\\beta} = \\frac{-1}{2}\\partial_{t}(g_{\\alpha\\beta})$.\n",
    "\n",
    "This metric is a solution to vacuum so the stress-energy tensor is: $T_{\\alpha\\beta} = 0$.\n",
    "\n",
    "Let's create arrays of this metric and extrinsic curvature. \n",
    "\n",
    "#### Create a data grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "6025d18e",
   "metadata": {},
   "outputs": [],
   "source": [
    "L = 20  # Data box size\n",
    "N = 32  # Number of data points per side\n",
    "dx = L/N  # Elementary grid size\n",
    "\n",
    "# Cartesian coordinates\n",
    "xyz = np.arange(-L/2, L/2, dx)\n",
    "x, y, z = np.meshgrid(xyz, xyz, xyz, indexing='ij')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "101901a2",
   "metadata": {},
   "source": [
    "#### Create metric tensor array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "666a0189",
   "metadata": {},
   "outputs": [],
   "source": [
    "t = 1.5  # An arbitrary time\n",
    "B = (x+np.log(t))  # Function from referenced book\n",
    "Box_0 = np.zeros([N, N, N])\n",
    "Box_1 = np.ones([N, N, N])\n",
    "\n",
    "gdown4 = np.array([[-Box_1, Box_0, Box_0, Box_0],\n",
    "                   [Box_0, (t*t)*Box_1, Box_0, Box_0],\n",
    "                   [Box_0, Box_0, t*np.exp(x), t*np.exp(x)*B],\n",
    "                   [Box_0, Box_0, t*np.exp(x)*B, \n",
    "                    t*np.exp(x)*(B*B+1)]])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2ed5fb87",
   "metadata": {},
   "source": [
    "#### Create extrinsic curvature tensor array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "009fd5d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "dtB = 1/t  # Time derivative of B function\n",
    "# Time derivative of metric\n",
    "dtgdown4 = np.array([[Box_0, Box_0, Box_0, Box_0],\n",
    "                     [Box_0, 2*t*Box_1, Box_0, Box_0],\n",
    "                     [Box_0, Box_0, np.exp(x), \n",
    "                      np.exp(x)*B + t*np.exp(x)*dtB],\n",
    "                     [Box_0, Box_0, np.exp(x)*B + t*np.exp(x)*dtB, \n",
    "                      np.exp(x)*(B*B+1) + t*np.exp(x)*(2*dtB*B)]])\n",
    "\n",
    "Kdown4 = (-1/2)*dtgdown4"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3af80b2",
   "metadata": {},
   "source": [
    "#### Create stress-energy tensor array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "9ffdc811",
   "metadata": {},
   "outputs": [],
   "source": [
    "kappa = 8*np.pi  # Einstein's constant with G = c = 1\n",
    "Tdown4 = np.zeros([4, 4, N, N, N])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5eb4886",
   "metadata": {},
   "source": [
    "# Electric $E_{\\alpha\\beta}$ and magnetic $B_{\\alpha\\beta}$ parts of the Weyl tensor\n",
    "\n",
    "#### Define the FiniteDifference class\n",
    "\n",
    "Default value are : periodic_boundary=True, fd_order6=False\n",
    "\n",
    "But periodic boundaries would be inapropriate for this spacetime, the code will then use a combination of forward centered and backward finite difference schemes instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "8e3297e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "FD = ebw.FiniteDifference(dx, N, periodic_boundary=False, \n",
    "                          fd_order6=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4151dee",
   "metadata": {},
   "source": [
    "#### Define the Weyl class\n",
    "\n",
    "Simply pass on the FD class we won't use it again here, but you can use it on it's own to compute spatial derives."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "076a1d1e",
   "metadata": {},
   "outputs": [],
   "source": [
    "EBW = ebw.Weyl(FD, gdown4, Kdown4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93188745",
   "metadata": {},
   "source": [
    "This will automatically compute the standard terms of the 3+1 formulation, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "3680e839",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "lapse =  1.0\n"
     ]
    }
   ],
   "source": [
    "print('lapse = ', np.mean(EBW.alpha))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75d26fd4",
   "metadata": {},
   "source": [
    "Notice that here I'm showing the mean. This is because np.shape(EBW.alpha) = (N, N, N) as the lapse can depend on space.\n",
    "\n",
    "#### Compute Spatial Ricci tensor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "8f9d889f",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gudd3 = EBW.christoffel_symbol_udd3()\n",
    "RicciTdown3 = EBW.ricci_tensor_down3(Gudd3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35ed9dad",
   "metadata": {},
   "source": [
    "This then easily provides the spatial Ricci scalar:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "ec896edb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "R3 =  -0.9045346830449191\n",
      "Analytical answer:  -0.8888888888888888\n"
     ]
    }
   ],
   "source": [
    "RicciS3 = EBW.trace_rank2tensor3(RicciTdown3)\n",
    "print('R3 = ', np.mean(FD.cutoffmask(RicciS3)))\n",
    "print('Analytical answer: ', -2/t**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6186cb00",
   "metadata": {},
   "source": [
    "Notice that here I'm applying the FD.cutoffmask function before taking the mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "4996d77f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on method cutoffmask in module ebweyl:\n",
      "\n",
      "cutoffmask(f) method of ebweyl.FiniteDifference instance\n",
      "    Remove points affected by the boundary condition.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "help(FD.cutoffmask)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20ee2a36",
   "metadata": {},
   "source": [
    "The backward and forward finite difference schemes slightly underperform compared to the centered one so that is why I remove those before taking the mean. I wouldn't bother doing this if I were using periodic boundary conditions.\n",
    "\n",
    "#### Compute $E_{\\alpha\\beta}$ and $B_{\\alpha\\beta}$ projected along the normal to the hypersurface"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "6245a19f",
   "metadata": {},
   "outputs": [],
   "source": [
    "Endown3 = EBW.eweyl_n_tensor_down3(RicciTdown3, kappa, Tdown4)\n",
    "Bndown3 = EBW.bweyl_n_tensor_down3(Gudd3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e0ef6e6",
   "metadata": {},
   "source": [
    "These are both covariant tensors with both indices being spatial, hence the 3 at the end. \n",
    "\n",
    "If I want the indices to include time as well I can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "f2c62429",
   "metadata": {},
   "outputs": [],
   "source": [
    "Endown4 = EBW.ebweyl_n_3D_to_4D(Endown3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aaa9f76d",
   "metadata": {},
   "source": [
    "Lets look at their norm : $|E| = \\sqrt{E_{\\alpha\\beta}E^{\\alpha\\beta}} = \\sqrt{E_{ij}E^{ij}}$. Here Greek indices are spacetime and Latin indices are only space.\n",
    "\n",
    "As this spacetime has a plane wave we have $|E|=|B|$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "7c14c083",
   "metadata": {},
   "outputs": [],
   "source": [
    "En_norm = EBW.norm_rank2tensor3(Endown3)\n",
    "# or = EBW.norm_rank2tensor4(Endown4)\n",
    "Bn_norm = EBW.norm_rank2tensor3(Bndown3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "fa39877e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "E^2 =  0.3195128272968852\n",
      "B^2 =  0.31372229195447765\n",
      "Analytical answer:  0.31426968052735443\n"
     ]
    }
   ],
   "source": [
    "print('E^2 = ', np.mean(FD.cutoffmask(En_norm)))\n",
    "print('B^2 = ', np.mean(FD.cutoffmask(Bn_norm)))\n",
    "print('Analytical answer: ', 1/(np.sqrt(2)*t**2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b453d7cb",
   "metadata": {},
   "source": [
    "### $E_{\\alpha\\beta}$ and $B_{\\alpha\\beta}$ along a different vector\n",
    "\n",
    "With the terms projected along the normal to the hypersurface we can construct the Weyl tensor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "50dd422d",
   "metadata": {},
   "outputs": [],
   "source": [
    "Cdown4 = EBW.weyl_tensor_down4(Endown3, Bndown3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0fb373e9",
   "metadata": {},
   "source": [
    "This can then be projected along any time-like vector. We chose the 4-velocity, for this metric it simply corresponds to the normal to the hypersurface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "89e69ad5",
   "metadata": {},
   "outputs": [],
   "source": [
    "uup4 = EBW.nup4  # spacetime indices up"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c0876e3",
   "metadata": {},
   "source": [
    "The the new $E_{\\alpha\\beta}$ and $B_{\\alpha\\beta}$ are obtained as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "1932af71",
   "metadata": {},
   "outputs": [],
   "source": [
    "Eudown4 = EBW.eweyl_u_tensor_down4(Cdown4, uup4)\n",
    "Budown4 = EBW.bweyl_u_tensor_down4(Cdown4, uup4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e97588d1",
   "metadata": {},
   "source": [
    "Because the 4-velocity here corresponds to the normal to the hypersurface, I demonstrate here that $|E|$ and $|B|$ are the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "fe07da56",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "|E| =  0.3195128272968852\n",
      "|B| =  0.313722291954477\n",
      "Analytical answer:  0.31426968052735443\n"
     ]
    }
   ],
   "source": [
    "Eu_norm = EBW.norm_rank2tensor4(Eudown4)\n",
    "Bu_norm = EBW.norm_rank2tensor4(Budown4)\n",
    "print('|E| = ', np.mean(FD.cutoffmask(Eu_norm)))\n",
    "print('|B| = ', np.mean(FD.cutoffmask(Bu_norm)))\n",
    "print('Analytical answer: ', 1/(np.sqrt(2)*t**2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c78c8d7",
   "metadata": {},
   "source": [
    "# Petrov type\n",
    "\n",
    "To find the Petrov type I construct the Weyl scalars $\\Psi$ and then compute the scalar invariants, (J, I, L, K, N). Those are then used to determine the Petrov type according to Figure 9.1 of \"Exact Solutions to Einstein's Field Equations\" 2003, 2nd edition, by H.Stephani, D.Kramer, M.MacCallum, C.Hoenselaers and E.Herlt.\n",
    "\n",
    "In order to compute the $\\Psi$s I need the full Weyl tensor provided in cell [17]."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "c294c996",
   "metadata": {},
   "outputs": [],
   "source": [
    "Psis = EBW.weyl_psi_scalars(Cdown4)\n",
    "invars = EBW.invariant_scalars(Psis)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "438609d2",
   "metadata": {},
   "source": [
    "If $I^3 - 27 J^2 = 0$ it means that this spacetime is special, either of type II, D, III, N, or O,\n",
    "\n",
    "otherwise it is of type I, the most general type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "6250f01c",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.042925637439787195"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "val = invars['I']**3 - 27*invars['J']**2\n",
    "np.mean(FD.cutoffmask(val**(1/6)).real)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a3b11e6",
   "metadata": {},
   "source": [
    "Then it is a special type. \n",
    "Note: \n",
    "- here I put the value to the power of 1/6, that is to show a value of the same order of magnitude as the Weyl tensor. \n",
    "- here we are only considering the real part, to do this properly the imaginary part also should be considered.\n",
    "- again I'm showing the mean, that is because this spacetime is homogeneous.\n",
    "- this value will not be exactly zero, most importantly it will tend towards zero as we increase the resolution. One also would need to determine the numerical error with 2 other results with different resolutions.\n",
    "\n",
    "If $I = J = 0$ then the spacetime is either of type III, N, or O, otherwise it is of type II or D."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "e91eaa6f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I^{1/2} =  0.04281828309503702\n",
      "J^{1/3} =  0.01033110076615475\n"
     ]
    }
   ],
   "source": [
    "print('I^{1/2} = ', np.mean(FD.cutoffmask(invars['I']**(1/2)).real))\n",
    "print('J^{1/3} = ', np.mean(FD.cutoffmask(invars['J']**(1/3)).real))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a33a03d2",
   "metadata": {},
   "source": [
    "Then it is of type III, N, or O.\n",
    "\n",
    "Next if $K = L = 0$ then the spacetime is of type N or O, otherwise it is on type III."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "8cb130ce",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "K^{1/3} =  3.4545726620063575e-07\n",
      "L^{1/2} =  0.0012214490053415914\n"
     ]
    }
   ],
   "source": [
    "print('K^{1/3} = ', np.mean(FD.cutoffmask(invars['K']**(1/3)).real))\n",
    "print('L^{1/2} = ', np.mean(FD.cutoffmask(invars['L']**(1/2)).real))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce212b1c",
   "metadata": {},
   "source": [
    "Then it is of type N, or O.\n",
    "\n",
    "Next if $|B| = |E| = 0$ then the spacetime is of type O, it is conformally flat, otherwise it is on type N."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "93177690",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "|B| =  0.313722291954477\n",
      "|E| =  0.3195128272968852\n"
     ]
    }
   ],
   "source": [
    "print('|B| = ', np.mean(FD.cutoffmask(Bu_norm)))\n",
    "print('|E| = ', np.mean(FD.cutoffmask(Eu_norm)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92cf29f8",
   "metadata": {},
   "source": [
    "As we have shown previously $|B| = |E| \\neq 0$ therefore this spacetime is of Petrov type N."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "63068868",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2fa13d68",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
